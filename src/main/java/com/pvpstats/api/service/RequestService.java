package com.pvpstats.api.service;

import com.pvpstats.api.domain.PvpBracketStatisticsResponse;
import com.pvpstats.api.domain.PvpLeaderboardResponse;
import com.pvpstats.api.domain.RealmsResponse;
import com.pvpstats.api.model.*;

import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class RequestService {

    public OAuth2ProtectedResourceDetails resourceDetails() {

        // Init client credentials resources
        ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
        resourceDetails.setAccessTokenUri("https://eu.battle.net/oauth/token");
        resourceDetails.setClientId("d1ed223aa515482e9295995485d68f43");
        resourceDetails.setClientSecret("KioSaOefjzi5zBI3nN61giuGcH7asJPq");
        resourceDetails.setGrantType("client_credentials");

        return resourceDetails;
    }

    public PvpBracketStatisticsResponse pvpBracketStatisticsRequest(String region, String realmSlug, String characterName, String pvpBracket, String namespace) throws IOException {

        PvpBracketStatistics pvpBracketStatistics = new PvpBracketStatistics(region, realmSlug, characterName, pvpBracket, namespace);

        DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails(), clientContext);

        // Set local variable from constructor to build http request
        region = pvpBracketStatistics.getRegion();
        realmSlug = pvpBracketStatistics.getRealmSlug();
        characterName = pvpBracketStatistics.getCharacterName();
        pvpBracket = pvpBracketStatistics.getPvpBracket();

        // Check if the region is EU or US
        namespace = region.equals("eu") ? pvpBracketStatistics.getNamespace() : "profile-us";

        // Concat variable to build the URI
        String url = "https://" + region + ".api.blizzard.com/profile/wow/character/" +
                realmSlug + "/" +
                characterName + "/pvp-bracket/" +
                pvpBracket + "?namespace=" +
                namespace + "&locale=en_US&access_token=" +
                restTemplate.getAccessToken();

        // Get the json from the http request
        String responseRequest = restTemplate.getForObject(url, String.class);

        PvpBracketStatisticsService pvpBracketStatisticsService = new PvpBracketStatisticsService();

        // Parse the json to get specific values
        return pvpBracketStatisticsService.GetJsonValuesFromPvpBracketStatistics(responseRequest);
    }

    public PvpLeaderboardResponse pvpLeaderboardRequest(String region, int pvpSeasonId, String pvpBracket, String namespace) throws IOException {

        PvpLeaderboard pvpLeaderboard = new PvpLeaderboard(region, pvpSeasonId, pvpBracket, namespace);

        DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails(), clientContext);

        // Set local variable from constructor to build http request
        region = pvpLeaderboard.getRegion();
        pvpBracket = pvpLeaderboard.getPvpBracket();

        // Check if the region is EU or US
        namespace = region.equals("eu") ? pvpLeaderboard.getNamespace() : "dynamic-us";

        // Concat variable to build the URI
        String url = "https://" + region + ".api.blizzard.com/data/wow/pvp-season/" +
                pvpSeasonId + "/" +
                "pvp-leaderboard/" +
                pvpBracket + "?namespace=" +
                namespace + "&locale=en_US&access_token=" +
                restTemplate.getAccessToken();

        // Get the json from the http request
        String responseRequest = restTemplate.getForObject(url, String.class);

        PvpLeaderBoardService pvpLeaderBoardService = new PvpLeaderBoardService();

        // Parse the json to get specific values
        return pvpLeaderBoardService.GetJsonValuesFromPvpLeaderboard(responseRequest);
    }

    public RealmsResponse realmsRequest(String region, String namespace) throws IOException {

        Realms realms = new Realms(region, namespace);

        DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails(), clientContext);

        // Set local variable from constructor to build http request
        region = realms.getRegion();

        // Check if the region is EU or US
        namespace = region.equals("eu") ? realms.getNamespace() : "dynamic-us";

        // Concat variable to build the URI
        String url = "https://" + region + ".api.blizzard.com/data/wow/realm/index" +
                "?namespace=" +
                namespace + "&locale=en_US&access_token=" +
                restTemplate.getAccessToken();

        // Get the json from the http request
        String responseRequest = restTemplate.getForObject(url, String.class);

        RealmsService realmsService = new RealmsService();

        // Parse the json to get specific values
        return realmsService.GetJsonValuesFromRealms(responseRequest);
    }
}