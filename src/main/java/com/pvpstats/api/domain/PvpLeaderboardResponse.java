package com.pvpstats.api.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class PvpLeaderboardResponse {

    private int pvpSeasonId;
    private String name;
    private List<Object> entries;
}