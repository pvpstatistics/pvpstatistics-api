package com.pvpstats.api.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class PvpBracketStatisticsResponse {

    private String characterName;
    private String realmName;
    private String factionName;
    private int rating;
    private int seasonMatchWon;
    private int seasonMatchLost;
    private int seasonMatchWinrate;
    private int weeklyMatchWon;
    private int weeklyMatchLost;
    private int weeklyMatchWinrate;
}