package com.pvpstats.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PvpstatsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PvpstatsApiApplication.class, args);
	}
}